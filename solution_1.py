def is_anagram(first_word, second_word):
    return sorted(first_word) == sorted(second_word)

print(is_anagram("life", "efil"))
print(is_anagram("life", "fine"))

