def factorial(n):
    if n == 0:
        return 1
    mult = 1
    for i in range(1, n + 1):
        mult *= i
    return mult


def calculate_pi(k):
    result = 0
    for i in range(1, k + 1):
        result += 4 * (-1) ** (i + 1) / (2 * i - 1)
    return result


def calculate_e(k):
    result = 0
    for i in range(k):
        result += 1 / factorial(i)
    return result


def calculate_pi_alternative(k):
    result = 1
    for i in range(1, k + 1):
        result *= (4 * i ** 2 / (4 * i ** 2 - 1))
    return 2 * result


print(calculate_pi(100000))
print(calculate_e(1000))
print(calculate_pi_alternative(100000))
