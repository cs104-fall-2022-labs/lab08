def get_vector_defined_by_two_points(point_p, point_q):
    return [point_q[i] - point_p[i] for i in range(len(point_p))]


print(get_vector_defined_by_two_points((1, 4, 6), (5, 6, 2)))
